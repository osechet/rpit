A server that executes mouse and keyboard commands from remote.

[![codecov](https://codecov.io/bb/osechet/rpit/branch/master/graph/badge.svg)](https://codecov.io/bb/osechet/rpit)

## Development ##

### Build ###

Get dependencies:
```
go get olivierscode.com/rpit/cmd/rpit-server
```

Build:
```
go install -v olivierscode.com/rpit/cmd/rpit-server
```

### Swagger ###

Install Swagger following one of the installation methods described here: https://github.com/go-swagger/go-swagger

```
swagger validate src/rpit-swagger.yaml
swagger generate server -f src/rpit-swagger.yaml -t src/olivierscode.com/rpit -A rpit
```