package helpers

import "olivierscode.com/rpit/models"

func NewProfile(id int64, name string) *models.Profile {
	return &models.Profile{ID: id, Name: &name, Pages: nil}
}

func NewPage(id int64, name string) *models.Page {
	return &models.Page{ID: id, Name: &name}
}
