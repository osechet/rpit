package restapi

import (
	"crypto/tls"
	"net/http"
	"os"

	errors "github.com/go-openapi/errors"
	runtime "github.com/go-openapi/runtime"
	middleware "github.com/go-openapi/runtime/middleware"
	logging "github.com/op/go-logging"
	graceful "github.com/tylerb/graceful"

	"olivierscode.com/rpit/database"
	"olivierscode.com/rpit/restapi/operations"
	"olivierscode.com/rpit/restapi/operations/page"
	"olivierscode.com/rpit/restapi/operations/profile"
)

// This file is safe to edit. Once it exists it will not be overwritten

//go:generate swagger generate server --target ../src/olivierscode.com/rpit --name rpit --spec ../src/osechet-rpit-1.0.0-swagger.yaml

var logger = logging.MustGetLogger("restapi")

var format = logging.MustStringFormatter(
	`%{color}%{time:15:04:05.000} %{level:.4s} ▶ %{module} %{shortfunc} %{id:03x}%{color:reset} %{message}`,
)

var db database.Database

func initDB(newDb database.Database) {
	db = newDb
}

func configureFlags(api *operations.RpitAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.RpitAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// s.api.Logger = log.Printf

	backend := logging.NewLogBackend(os.Stderr, "", 0)
	backendFormatter := logging.NewBackendFormatter(backend, format)
	backendLeveled := logging.AddModuleLevel(backendFormatter)
	backendLeveled.SetLevel(logging.DEBUG, "")
	logging.SetBackend(backendLeveled)

	initDB(database.NewBasicDatabase())

	api.Logger = logger.Debugf

	api.JSONConsumer = runtime.JSONConsumer()

	api.XMLConsumer = runtime.XMLConsumer()

	api.JSONProducer = runtime.JSONProducer()

	api.XMLProducer = runtime.XMLProducer()

	api.ProfileAddProfileHandler = profile.AddProfileHandlerFunc(addProfileHandler)
	api.ProfileDeleteProfileHandler = profile.DeleteProfileHandlerFunc(deleteProfileHandler)
	api.ProfileFindProfilesHandler = profile.FindProfilesHandlerFunc(findProfilesHandler)
	api.ProfileGetActiveProfileHandler = profile.GetActiveProfileHandlerFunc(getActiveProfileHandler)
	api.ProfileGetProfileByIDHandler = profile.GetProfileByIDHandlerFunc(getProfileByIDHandler)
	api.ProfileSetActiveProfileHandler = profile.SetActiveProfileHandlerFunc(setActiveProfileHandler)
	api.ProfileUpdateProfileHandler = profile.UpdateProfileHandlerFunc(updateProfileHandler)
	api.PageAddPageHandler = page.AddPageHandlerFunc(addPageHandler)
	api.PageDeletePageHandler = page.DeletePageHandlerFunc(deletePageHandler)
	api.PageFindPagesHandler = page.FindPagesHandlerFunc(findPagesHandler)
	api.PageGetPageByIDHandler = page.GetPageByIDHandlerFunc(getPageByIDHandler)
	api.PageUpdatePageHandler = page.UpdatePageHandlerFunc(updatePageHandler)

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *graceful.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}

// Handlers

func addProfileHandler(params profile.AddProfileParams) middleware.Responder {
	newProfile, err := db.AddProfile(params.Body)
	if err != nil {
		return profile.NewAddProfileBadRequest()
	}
	return profile.NewAddProfileCreated().WithPayload(newProfile)
}

func deleteProfileHandler(params profile.DeleteProfileParams) middleware.Responder {
	if params.ProfileID == 0 {
		return profile.NewDeleteProfileBadRequest()
	}
	err := db.DeleteProfile(params.ProfileID)
	if err != nil {
		return profile.NewDeleteProfileNotFound()
	}
	return profile.NewDeleteProfileOK()
}

func findProfilesHandler(params profile.FindProfilesParams) middleware.Responder {
	profiles := db.FindProfiles()
	return profile.NewFindProfilesOK().WithPayload(profiles)
}

func getActiveProfileHandler(params profile.GetActiveProfileParams) middleware.Responder {
	activeProfile := db.GetActiveProfile()
	if activeProfile == nil {
		return profile.NewGetActiveProfileNotFound()
	}
	return profile.NewGetActiveProfileOK().WithPayload(activeProfile)
}

func getProfileByIDHandler(params profile.GetProfileByIDParams) middleware.Responder {
	if params.ProfileID == 0 {
		return profile.NewGetProfileByIDBadRequest()
	}
	profileByID := db.GetProfileByID(params.ProfileID)
	if profileByID == nil {
		return profile.NewGetProfileByIDNotFound()
	}
	return profile.NewGetProfileByIDOK().WithPayload(profileByID)
}

func setActiveProfileHandler(params profile.SetActiveProfileParams) middleware.Responder {
	if params.ProfileID == 0 {
		return profile.NewSetActiveProfileBadRequest()
	}
	p, err := db.SetActiveProfile(params.ProfileID)
	if err != nil {
		return profile.NewSetActiveProfileNotFound()
	}
	return profile.NewSetActiveProfileOK().WithPayload(p)
}

func updateProfileHandler(params profile.UpdateProfileParams) middleware.Responder {
	if params.Body.ID == 0 {
		return profile.NewUpdateProfileBadRequest()
	}
	p, err := db.UpdateProfile(params.Body)
	if err != nil {
		return profile.NewUpdateProfileNotFound()
	}
	return profile.NewUpdateProfileOK().WithPayload(p)
}

func addPageHandler(params page.AddPageParams) middleware.Responder {
	newPage, err := db.AddPage(params.ProfileID, params.Body)
	if err != nil {
		return page.NewAddPageBadRequest()
	}
	return page.NewAddPageCreated().WithPayload(newPage)
}

func deletePageHandler(params page.DeletePageParams) middleware.Responder {
	if params.PageID == 0 {
		return page.NewDeletePageBadRequest()
	}
	err := db.DeletePage(params.ProfileID, params.PageID)
	if err != nil {
		return page.NewDeletePageNotFound()
	}
	return page.NewDeletePageOK()
}

func findPagesHandler(params page.FindPagesParams) middleware.Responder {
	return middleware.NotImplemented("operation page.FindPages has not yet been implemented")
}

func getPageByIDHandler(params page.GetPageByIDParams) middleware.Responder {
	return middleware.NotImplemented("operation page.GetPageByID has not yet been implemented")
}

func updatePageHandler(params page.UpdatePageParams) middleware.Responder {
	return middleware.NotImplemented("operation page.UpdatePage has not yet been implemented")
}
