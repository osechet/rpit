/*Package restapi rPit

This is an Remote Programmable Input Trigger (rPit) server.



    Schemes:
      http
      https
    Host: localhost
    BasePath: /api/rpit/1.0.0
    Version: 1.0.0
    License: Apache 2.0 http://www.apache.org/licenses/LICENSE-2.0.html
    Contact: <osechet@gmail.com>

    Consumes:
    - application/json

    - application/xml


    Produces:
    - application/json

    - application/xml


swagger:meta
*/
package restapi
