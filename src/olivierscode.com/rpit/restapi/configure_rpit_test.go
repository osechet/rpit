package restapi

import (
	"errors"
	"fmt"
	"testing"

	"github.com/golang/mock/gomock"
	. "github.com/smartystreets/goconvey/convey"
	"olivierscode.com/rpit/database/mock_database"
	"olivierscode.com/rpit/helpers"
	"olivierscode.com/rpit/models"
	"olivierscode.com/rpit/restapi/operations/profile"
)

func Test_addProfileHandler(t *testing.T) {
	Convey("AddProfile handler", t, func() {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mockDb := mock_database.NewMockDatabase(ctrl)
		initDB(mockDb)

		Convey("Given a valid parameter", func() {
			p := helpers.NewProfile(1, "profile 1")
			params := profile.NewAddProfileParams()
			params.Body = p

			Convey("Given a database", func() {
				mockDb.EXPECT().AddProfile(p).Return(p, nil)

				Convey("When the AddProfile handler is called", func() {
					got := addProfileHandler(params)

					Convey("It should succeed", func() {
						So(got, ShouldResemble, profile.NewAddProfileCreated().WithPayload(p))
					})
				})
			})
		})

		Convey("Given an invalid parameter", func() {
			params := profile.NewAddProfileParams()

			Convey("Given a valid database", func() {
				mockDb.EXPECT().AddProfile(nil).Return(nil, errors.New("any"))

				Convey("When the AddProfile handler is called", func() {
					got := addProfileHandler(params)

					Convey("It should return a bad request error", func() {
						So(got, ShouldResemble, profile.NewAddProfileBadRequest())
					})
				})
			})
		})
	})
}
func Test_deleteProfileHandler(t *testing.T) {
	Convey("DeleteProfile handler", t, func() {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mockDb := mock_database.NewMockDatabase(ctrl)
		initDB(mockDb)

		Convey("Given a valid parameter", func() {
			id := int64(1)
			params := profile.NewDeleteProfileParams()
			params.ProfileID = id

			Convey("Given a database containing the queried profile", func() {
				mockDb.EXPECT().DeleteProfile(id).Return(nil)

				Convey("When the DeleteProfile handler is called", func() {
					got := deleteProfileHandler(params)

					Convey("It should succeed", func() {
						So(got, ShouldResemble, profile.NewDeleteProfileOK())
					})
				})
			})

			Convey("Given a database not containing the queried profile", func() {
				mockDb.EXPECT().DeleteProfile(id).Return(errors.New("any"))

				Convey("When the DeleteProfile handler is called", func() {
					got := deleteProfileHandler(params)

					Convey("It should return a not found error", func() {
						So(got, ShouldResemble, profile.NewDeleteProfileNotFound())
					})
				})
			})
		})

		Convey("Given an invalid parameter", func() {
			params := profile.NewDeleteProfileParams()

			Convey("When the DeleteProfile handler is called", func() {
				got := deleteProfileHandler(params)

				Convey("It should return a bad request error", func() {
					So(got, ShouldResemble, profile.NewDeleteProfileBadRequest())
				})
			})
		})
	})
}
func Test_findProfilesHandler(t *testing.T) {
	Convey("FindProfiles handler", t, func() {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mockDb := mock_database.NewMockDatabase(ctrl)
		initDB(mockDb)

		Convey("Given an empty database", func() {
			mockDb.EXPECT().FindProfiles().Return(nil)

			Convey("When the FindProfiles handler is called", func() {
				got := findProfilesHandler(profile.NewFindProfilesParams())

				Convey("It should return a response with an empty array", func() {
					So(got, ShouldResemble, profile.NewFindProfilesOK())
				})
			})
		})

		Convey("Given a not-empty database", func() {
			profiles := make([]*models.Profile, 4)
			for i := range profiles {
				id := int64(i + 1)
				profiles[i] = helpers.NewProfile(id, fmt.Sprintf("profile %d", id))
			}
			mockDb.EXPECT().FindProfiles().Return(profiles)

			Convey("When the FindProfiles handler is called", func() {
				got := findProfilesHandler(profile.NewFindProfilesParams())

				Convey("It should return a response with the data from the database", func() {
					So(got, ShouldResemble, profile.NewFindProfilesOK().WithPayload(profiles))
				})
			})
		})
	})
}
func Test_getActiveProfileHandler(t *testing.T) {
	Convey("GetActiveProfile handler", t, func() {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mockDb := mock_database.NewMockDatabase(ctrl)
		initDB(mockDb)

		Convey("Given a database with no active profile", func() {
			mockDb.EXPECT().GetActiveProfile().Return(nil)

			Convey("When the GetActiveProfile handler is called", func() {
				got := getActiveProfileHandler(profile.NewGetActiveProfileParams())

				Convey("It should return a response with an empty array", func() {
					So(got, ShouldResemble, profile.NewGetActiveProfileNotFound())
				})
			})
		})

		Convey("Given a not-empty database", func() {
			p := helpers.NewProfile(1, "profile 1")
			mockDb.EXPECT().GetActiveProfile().Return(p)

			Convey("When the GetActiveProfile handler is called", func() {
				got := getActiveProfileHandler(profile.NewGetActiveProfileParams())

				Convey("It should return a response with the data from the database", func() {
					So(got, ShouldResemble, profile.NewGetActiveProfileOK().WithPayload(p))
				})
			})
		})
	})
}
func Test_getProfileByIDHandler(t *testing.T) {
	Convey("GetProfileByID handler", t, func() {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mockDb := mock_database.NewMockDatabase(ctrl)
		initDB(mockDb)

		Convey("Given a valid parameter", func() {
			id := int64(1)
			params := profile.NewGetProfileByIDParams()
			params.ProfileID = id

			Convey("Given a database containing the queried profile", func() {
				p := helpers.NewProfile(1, "profile 1")
				mockDb.EXPECT().GetProfileByID(id).Return(p)

				Convey("When the GetProfileByID handler is called", func() {
					got := getProfileByIDHandler(params)

					Convey("It should return a response with the queried profile", func() {
						So(got, ShouldResemble, profile.NewGetProfileByIDOK().WithPayload(p))
					})
				})
			})

			Convey("Given a database not containing the queried profile", func() {
				mockDb.EXPECT().GetProfileByID(id).Return(nil)

				Convey("When the GetProfileByID handler is called", func() {
					got := getProfileByIDHandler(params)

					Convey("It should return a not found error", func() {
						So(got, ShouldResemble, profile.NewGetProfileByIDNotFound())
					})
				})
			})
		})

		Convey("Given an invalid parameter", func() {
			params := profile.NewGetProfileByIDParams()

			Convey("When the GetProfileByID handler is called", func() {
				got := getProfileByIDHandler(params)

				Convey("It should return a bad request error", func() {
					So(got, ShouldResemble, profile.NewGetProfileByIDBadRequest())
				})
			})
		})
	})
}
func Test_setActiveProfileHandler(t *testing.T) {
	Convey("SetActiveProfile handler", t, func() {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mockDb := mock_database.NewMockDatabase(ctrl)
		initDB(mockDb)

		Convey("Given a valid parameter", func() {
			id := int64(1)
			params := profile.NewSetActiveProfileParams()
			params.ProfileID = id

			Convey("Given a database containing the queried profile", func() {
				p := helpers.NewProfile(1, "profile 1")
				mockDb.EXPECT().SetActiveProfile(id).Return(p, nil)

				Convey("When the SetActiveProfile handler is called", func() {
					got := setActiveProfileHandler(params)

					Convey("It should return a response with the queried profile", func() {
						So(got, ShouldResemble, profile.NewSetActiveProfileOK().WithPayload(p))
					})
				})
			})

			Convey("Given a database not containing the queried profile", func() {
				mockDb.EXPECT().SetActiveProfile(id).Return(nil, errors.New("any"))

				Convey("When the SetActiveProfile handler is called", func() {
					got := setActiveProfileHandler(params)

					Convey("It should return a not found error", func() {
						So(got, ShouldResemble, profile.NewSetActiveProfileNotFound())
					})
				})
			})
		})

		Convey("Given an invalid parameter", func() {
			params := profile.NewSetActiveProfileParams()

			Convey("When the SelectProfile handler is called", func() {
				got := setActiveProfileHandler(params)

				Convey("It should return a bad request error", func() {
					So(got, ShouldResemble, profile.NewSetActiveProfileBadRequest())
				})
			})
		})
	})
}
func Test_updateProfileHandler(t *testing.T) {
	Convey("UpdateProfile handler", t, func() {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mockDb := mock_database.NewMockDatabase(ctrl)
		initDB(mockDb)

		Convey("Given a valid parameter", func() {
			p := helpers.NewProfile(1, "profile 1 updated")
			params := profile.NewUpdateProfileParams()
			params.Body = p

			Convey("Given a database containing the queried profile", func() {
				dbProfile := helpers.NewProfile(1, "profile 1")
				mockDb.EXPECT().UpdateProfile(p).Return(dbProfile, nil)

				Convey("When the UpdateProfile handler is called", func() {
					got := updateProfileHandler(params)

					Convey("It should return a response with the queried profile", func() {
						So(got, ShouldResemble, profile.NewUpdateProfileOK().WithPayload(dbProfile))
					})
				})
			})

			Convey("Given a database not containing the queried profile", func() {
				mockDb.EXPECT().UpdateProfile(p).Return(nil, errors.New("any"))

				Convey("When the UpdateProfile handler is called", func() {
					got := updateProfileHandler(params)

					Convey("It should return a not found error", func() {
						So(got, ShouldResemble, profile.NewUpdateProfileNotFound())
					})
				})
			})
		})

		Convey("Given an invalid parameter", func() {
			p := helpers.NewProfile(0, "profile 0 updated")
			params := profile.NewUpdateProfileParams()
			params.Body = p

			Convey("When the UpdateProfile handler is called", func() {
				got := updateProfileHandler(params)

				Convey("It should return a bad request error", func() {
					So(got, ShouldResemble, profile.NewUpdateProfileBadRequest())
				})
			})
		})
	})
}
