package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"net/http"
	"strings"

	errors "github.com/go-openapi/errors"
	loads "github.com/go-openapi/loads"
	runtime "github.com/go-openapi/runtime"
	middleware "github.com/go-openapi/runtime/middleware"
	spec "github.com/go-openapi/spec"
	strfmt "github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"

	"olivierscode.com/rpit/restapi/operations/page"
	"olivierscode.com/rpit/restapi/operations/profile"
)

// NewRpitAPI creates a new Rpit instance
func NewRpitAPI(spec *loads.Document) *RpitAPI {
	return &RpitAPI{
		handlers:        make(map[string]map[string]http.Handler),
		formats:         strfmt.Default,
		defaultConsumes: "application/json",
		defaultProduces: "application/json",
		ServerShutdown:  func() {},
		spec:            spec,
		ServeError:      errors.ServeError,
		JSONConsumer:    runtime.JSONConsumer(),
		XMLConsumer:     runtime.XMLConsumer(),
		JSONProducer:    runtime.JSONProducer(),
		XMLProducer:     runtime.XMLProducer(),
		PageAddPageHandler: page.AddPageHandlerFunc(func(params page.AddPageParams) middleware.Responder {
			return middleware.NotImplemented("operation PageAddPage has not yet been implemented")
		}),
		ProfileAddProfileHandler: profile.AddProfileHandlerFunc(func(params profile.AddProfileParams) middleware.Responder {
			return middleware.NotImplemented("operation ProfileAddProfile has not yet been implemented")
		}),
		PageDeletePageHandler: page.DeletePageHandlerFunc(func(params page.DeletePageParams) middleware.Responder {
			return middleware.NotImplemented("operation PageDeletePage has not yet been implemented")
		}),
		ProfileDeleteProfileHandler: profile.DeleteProfileHandlerFunc(func(params profile.DeleteProfileParams) middleware.Responder {
			return middleware.NotImplemented("operation ProfileDeleteProfile has not yet been implemented")
		}),
		PageFindPagesHandler: page.FindPagesHandlerFunc(func(params page.FindPagesParams) middleware.Responder {
			return middleware.NotImplemented("operation PageFindPages has not yet been implemented")
		}),
		ProfileFindProfilesHandler: profile.FindProfilesHandlerFunc(func(params profile.FindProfilesParams) middleware.Responder {
			return middleware.NotImplemented("operation ProfileFindProfiles has not yet been implemented")
		}),
		ProfileGetActiveProfileHandler: profile.GetActiveProfileHandlerFunc(func(params profile.GetActiveProfileParams) middleware.Responder {
			return middleware.NotImplemented("operation ProfileGetActiveProfile has not yet been implemented")
		}),
		PageGetPageByIDHandler: page.GetPageByIDHandlerFunc(func(params page.GetPageByIDParams) middleware.Responder {
			return middleware.NotImplemented("operation PageGetPageByID has not yet been implemented")
		}),
		ProfileGetProfileByIDHandler: profile.GetProfileByIDHandlerFunc(func(params profile.GetProfileByIDParams) middleware.Responder {
			return middleware.NotImplemented("operation ProfileGetProfileByID has not yet been implemented")
		}),
		ProfileSetActiveProfileHandler: profile.SetActiveProfileHandlerFunc(func(params profile.SetActiveProfileParams) middleware.Responder {
			return middleware.NotImplemented("operation ProfileSetActiveProfile has not yet been implemented")
		}),
		PageUpdatePageHandler: page.UpdatePageHandlerFunc(func(params page.UpdatePageParams) middleware.Responder {
			return middleware.NotImplemented("operation PageUpdatePage has not yet been implemented")
		}),
		ProfileUpdateProfileHandler: profile.UpdateProfileHandlerFunc(func(params profile.UpdateProfileParams) middleware.Responder {
			return middleware.NotImplemented("operation ProfileUpdateProfile has not yet been implemented")
		}),
	}
}

/*RpitAPI This is an Remote Programmable Input Trigger (rPit) server.
 */
type RpitAPI struct {
	spec            *loads.Document
	context         *middleware.Context
	handlers        map[string]map[string]http.Handler
	formats         strfmt.Registry
	defaultConsumes string
	defaultProduces string
	Middleware      func(middleware.Builder) http.Handler
	// JSONConsumer registers a consumer for a "application/json" mime type
	JSONConsumer runtime.Consumer
	// XMLConsumer registers a consumer for a "application/xml" mime type
	XMLConsumer runtime.Consumer

	// JSONProducer registers a producer for a "application/json" mime type
	JSONProducer runtime.Producer
	// XMLProducer registers a producer for a "application/xml" mime type
	XMLProducer runtime.Producer

	// PageAddPageHandler sets the operation handler for the add page operation
	PageAddPageHandler page.AddPageHandler
	// ProfileAddProfileHandler sets the operation handler for the add profile operation
	ProfileAddProfileHandler profile.AddProfileHandler
	// PageDeletePageHandler sets the operation handler for the delete page operation
	PageDeletePageHandler page.DeletePageHandler
	// ProfileDeleteProfileHandler sets the operation handler for the delete profile operation
	ProfileDeleteProfileHandler profile.DeleteProfileHandler
	// PageFindPagesHandler sets the operation handler for the find pages operation
	PageFindPagesHandler page.FindPagesHandler
	// ProfileFindProfilesHandler sets the operation handler for the find profiles operation
	ProfileFindProfilesHandler profile.FindProfilesHandler
	// ProfileGetActiveProfileHandler sets the operation handler for the get active profile operation
	ProfileGetActiveProfileHandler profile.GetActiveProfileHandler
	// PageGetPageByIDHandler sets the operation handler for the get page by Id operation
	PageGetPageByIDHandler page.GetPageByIDHandler
	// ProfileGetProfileByIDHandler sets the operation handler for the get profile by Id operation
	ProfileGetProfileByIDHandler profile.GetProfileByIDHandler
	// ProfileSetActiveProfileHandler sets the operation handler for the set active profile operation
	ProfileSetActiveProfileHandler profile.SetActiveProfileHandler
	// PageUpdatePageHandler sets the operation handler for the update page operation
	PageUpdatePageHandler page.UpdatePageHandler
	// ProfileUpdateProfileHandler sets the operation handler for the update profile operation
	ProfileUpdateProfileHandler profile.UpdateProfileHandler

	// ServeError is called when an error is received, there is a default handler
	// but you can set your own with this
	ServeError func(http.ResponseWriter, *http.Request, error)

	// ServerShutdown is called when the HTTP(S) server is shut down and done
	// handling all active connections and does not accept connections any more
	ServerShutdown func()

	// Custom command line argument groups with their descriptions
	CommandLineOptionsGroups []swag.CommandLineOptionsGroup

	// User defined logger function.
	Logger func(string, ...interface{})
}

// SetDefaultProduces sets the default produces media type
func (o *RpitAPI) SetDefaultProduces(mediaType string) {
	o.defaultProduces = mediaType
}

// SetDefaultConsumes returns the default consumes media type
func (o *RpitAPI) SetDefaultConsumes(mediaType string) {
	o.defaultConsumes = mediaType
}

// SetSpec sets a spec that will be served for the clients.
func (o *RpitAPI) SetSpec(spec *loads.Document) {
	o.spec = spec
}

// DefaultProduces returns the default produces media type
func (o *RpitAPI) DefaultProduces() string {
	return o.defaultProduces
}

// DefaultConsumes returns the default consumes media type
func (o *RpitAPI) DefaultConsumes() string {
	return o.defaultConsumes
}

// Formats returns the registered string formats
func (o *RpitAPI) Formats() strfmt.Registry {
	return o.formats
}

// RegisterFormat registers a custom format validator
func (o *RpitAPI) RegisterFormat(name string, format strfmt.Format, validator strfmt.Validator) {
	o.formats.Add(name, format, validator)
}

// Validate validates the registrations in the RpitAPI
func (o *RpitAPI) Validate() error {
	var unregistered []string

	if o.JSONConsumer == nil {
		unregistered = append(unregistered, "JSONConsumer")
	}

	if o.XMLConsumer == nil {
		unregistered = append(unregistered, "XMLConsumer")
	}

	if o.JSONProducer == nil {
		unregistered = append(unregistered, "JSONProducer")
	}

	if o.XMLProducer == nil {
		unregistered = append(unregistered, "XMLProducer")
	}

	if o.PageAddPageHandler == nil {
		unregistered = append(unregistered, "page.AddPageHandler")
	}

	if o.ProfileAddProfileHandler == nil {
		unregistered = append(unregistered, "profile.AddProfileHandler")
	}

	if o.PageDeletePageHandler == nil {
		unregistered = append(unregistered, "page.DeletePageHandler")
	}

	if o.ProfileDeleteProfileHandler == nil {
		unregistered = append(unregistered, "profile.DeleteProfileHandler")
	}

	if o.PageFindPagesHandler == nil {
		unregistered = append(unregistered, "page.FindPagesHandler")
	}

	if o.ProfileFindProfilesHandler == nil {
		unregistered = append(unregistered, "profile.FindProfilesHandler")
	}

	if o.ProfileGetActiveProfileHandler == nil {
		unregistered = append(unregistered, "profile.GetActiveProfileHandler")
	}

	if o.PageGetPageByIDHandler == nil {
		unregistered = append(unregistered, "page.GetPageByIDHandler")
	}

	if o.ProfileGetProfileByIDHandler == nil {
		unregistered = append(unregistered, "profile.GetProfileByIDHandler")
	}

	if o.ProfileSetActiveProfileHandler == nil {
		unregistered = append(unregistered, "profile.SetActiveProfileHandler")
	}

	if o.PageUpdatePageHandler == nil {
		unregistered = append(unregistered, "page.UpdatePageHandler")
	}

	if o.ProfileUpdateProfileHandler == nil {
		unregistered = append(unregistered, "profile.UpdateProfileHandler")
	}

	if len(unregistered) > 0 {
		return fmt.Errorf("missing registration: %s", strings.Join(unregistered, ", "))
	}

	return nil
}

// ServeErrorFor gets a error handler for a given operation id
func (o *RpitAPI) ServeErrorFor(operationID string) func(http.ResponseWriter, *http.Request, error) {
	return o.ServeError
}

// AuthenticatorsFor gets the authenticators for the specified security schemes
func (o *RpitAPI) AuthenticatorsFor(schemes map[string]spec.SecurityScheme) map[string]runtime.Authenticator {

	return nil

}

// ConsumersFor gets the consumers for the specified media types
func (o *RpitAPI) ConsumersFor(mediaTypes []string) map[string]runtime.Consumer {

	result := make(map[string]runtime.Consumer)
	for _, mt := range mediaTypes {
		switch mt {

		case "application/json":
			result["application/json"] = o.JSONConsumer

		case "application/xml":
			result["application/xml"] = o.XMLConsumer

		}
	}
	return result

}

// ProducersFor gets the producers for the specified media types
func (o *RpitAPI) ProducersFor(mediaTypes []string) map[string]runtime.Producer {

	result := make(map[string]runtime.Producer)
	for _, mt := range mediaTypes {
		switch mt {

		case "application/json":
			result["application/json"] = o.JSONProducer

		case "application/xml":
			result["application/xml"] = o.XMLProducer

		}
	}
	return result

}

// HandlerFor gets a http.Handler for the provided operation method and path
func (o *RpitAPI) HandlerFor(method, path string) (http.Handler, bool) {
	if o.handlers == nil {
		return nil, false
	}
	um := strings.ToUpper(method)
	if _, ok := o.handlers[um]; !ok {
		return nil, false
	}
	if path == "/" {
		path = ""
	}
	h, ok := o.handlers[um][path]
	return h, ok
}

// Context returns the middleware context for the rpit API
func (o *RpitAPI) Context() *middleware.Context {
	if o.context == nil {
		o.context = middleware.NewRoutableContext(o.spec, o, nil)
	}

	return o.context
}

func (o *RpitAPI) initHandlerCache() {
	o.Context() // don't care about the result, just that the initialization happened

	if o.handlers == nil {
		o.handlers = make(map[string]map[string]http.Handler)
	}

	if o.handlers["POST"] == nil {
		o.handlers["POST"] = make(map[string]http.Handler)
	}
	o.handlers["POST"]["/profile/{profileId}/page"] = page.NewAddPage(o.context, o.PageAddPageHandler)

	if o.handlers["POST"] == nil {
		o.handlers["POST"] = make(map[string]http.Handler)
	}
	o.handlers["POST"]["/profile"] = profile.NewAddProfile(o.context, o.ProfileAddProfileHandler)

	if o.handlers["DELETE"] == nil {
		o.handlers["DELETE"] = make(map[string]http.Handler)
	}
	o.handlers["DELETE"]["/profile/{profileId}/page/{pageId}"] = page.NewDeletePage(o.context, o.PageDeletePageHandler)

	if o.handlers["DELETE"] == nil {
		o.handlers["DELETE"] = make(map[string]http.Handler)
	}
	o.handlers["DELETE"]["/profile/{profileId}"] = profile.NewDeleteProfile(o.context, o.ProfileDeleteProfileHandler)

	if o.handlers["GET"] == nil {
		o.handlers["GET"] = make(map[string]http.Handler)
	}
	o.handlers["GET"]["/profile/{profileId}/page"] = page.NewFindPages(o.context, o.PageFindPagesHandler)

	if o.handlers["GET"] == nil {
		o.handlers["GET"] = make(map[string]http.Handler)
	}
	o.handlers["GET"]["/profile"] = profile.NewFindProfiles(o.context, o.ProfileFindProfilesHandler)

	if o.handlers["GET"] == nil {
		o.handlers["GET"] = make(map[string]http.Handler)
	}
	o.handlers["GET"]["/profile/active"] = profile.NewGetActiveProfile(o.context, o.ProfileGetActiveProfileHandler)

	if o.handlers["GET"] == nil {
		o.handlers["GET"] = make(map[string]http.Handler)
	}
	o.handlers["GET"]["/profile/{profileId}/page/{pageId}"] = page.NewGetPageByID(o.context, o.PageGetPageByIDHandler)

	if o.handlers["GET"] == nil {
		o.handlers["GET"] = make(map[string]http.Handler)
	}
	o.handlers["GET"]["/profile/{profileId}"] = profile.NewGetProfileByID(o.context, o.ProfileGetProfileByIDHandler)

	if o.handlers["GET"] == nil {
		o.handlers["GET"] = make(map[string]http.Handler)
	}
	o.handlers["GET"]["/profile/select/{profileId}"] = profile.NewSetActiveProfile(o.context, o.ProfileSetActiveProfileHandler)

	if o.handlers["PUT"] == nil {
		o.handlers["PUT"] = make(map[string]http.Handler)
	}
	o.handlers["PUT"]["/profile/{profileId}/page"] = page.NewUpdatePage(o.context, o.PageUpdatePageHandler)

	if o.handlers["PUT"] == nil {
		o.handlers["PUT"] = make(map[string]http.Handler)
	}
	o.handlers["PUT"]["/profile"] = profile.NewUpdateProfile(o.context, o.ProfileUpdateProfileHandler)

}

// Serve creates a http handler to serve the API over HTTP
// can be used directly in http.ListenAndServe(":8000", api.Serve(nil))
func (o *RpitAPI) Serve(builder middleware.Builder) http.Handler {
	o.Init()

	if o.Middleware != nil {
		return o.Middleware(builder)
	}
	return o.context.APIHandler(builder)
}

// Init allows you to just initialize the handler cache, you can then recompose the middelware as you see fit
func (o *RpitAPI) Init() {
	if len(o.handlers) == 0 {
		o.initHandlerCache()
	}
}
