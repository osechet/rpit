package page

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	"olivierscode.com/rpit/models"
)

// AddPageCreatedCode is the HTTP code returned for type AddPageCreated
const AddPageCreatedCode int = 201

/*AddPageCreated Created

swagger:response addPageCreated
*/
type AddPageCreated struct {

	/*
	  In: Body
	*/
	Payload *models.Page `json:"body,omitempty"`
}

// NewAddPageCreated creates AddPageCreated with default headers values
func NewAddPageCreated() *AddPageCreated {
	return &AddPageCreated{}
}

// WithPayload adds the payload to the add page created response
func (o *AddPageCreated) WithPayload(payload *models.Page) *AddPageCreated {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the add page created response
func (o *AddPageCreated) SetPayload(payload *models.Page) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *AddPageCreated) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(201)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// AddPageBadRequestCode is the HTTP code returned for type AddPageBadRequest
const AddPageBadRequestCode int = 400

/*AddPageBadRequest Invalid input

swagger:response addPageBadRequest
*/
type AddPageBadRequest struct {
}

// NewAddPageBadRequest creates AddPageBadRequest with default headers values
func NewAddPageBadRequest() *AddPageBadRequest {
	return &AddPageBadRequest{}
}

// WriteResponse to the client
func (o *AddPageBadRequest) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(400)
}

// AddPageNotFoundCode is the HTTP code returned for type AddPageNotFound
const AddPageNotFoundCode int = 404

/*AddPageNotFound Profile not found

swagger:response addPageNotFound
*/
type AddPageNotFound struct {
}

// NewAddPageNotFound creates AddPageNotFound with default headers values
func NewAddPageNotFound() *AddPageNotFound {
	return &AddPageNotFound{}
}

// WriteResponse to the client
func (o *AddPageNotFound) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(404)
}
