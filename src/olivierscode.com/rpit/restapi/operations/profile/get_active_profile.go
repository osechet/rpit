package profile

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	middleware "github.com/go-openapi/runtime/middleware"
)

// GetActiveProfileHandlerFunc turns a function with the right signature into a get active profile handler
type GetActiveProfileHandlerFunc func(GetActiveProfileParams) middleware.Responder

// Handle executing the request and returning a response
func (fn GetActiveProfileHandlerFunc) Handle(params GetActiveProfileParams) middleware.Responder {
	return fn(params)
}

// GetActiveProfileHandler interface for that can handle valid get active profile params
type GetActiveProfileHandler interface {
	Handle(GetActiveProfileParams) middleware.Responder
}

// NewGetActiveProfile creates a new http.Handler for the get active profile operation
func NewGetActiveProfile(ctx *middleware.Context, handler GetActiveProfileHandler) *GetActiveProfile {
	return &GetActiveProfile{Context: ctx, Handler: handler}
}

/*GetActiveProfile swagger:route GET /profile/active profile getActiveProfile

Return the active profile

*/
type GetActiveProfile struct {
	Context *middleware.Context
	Handler GetActiveProfileHandler
}

func (o *GetActiveProfile) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, _ := o.Context.RouteInfo(r)
	var Params = NewGetActiveProfileParams()

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
