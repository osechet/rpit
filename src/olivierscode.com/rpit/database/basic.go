package database

import (
	"sync"
	"sync/atomic"

	errors "github.com/go-openapi/errors"
	logging "github.com/op/go-logging"
	"olivierscode.com/rpit/models"
)

var log = logging.MustGetLogger("rpit")

// ByID can be used to sort slices of Profile.
type ByID []*models.Profile

func (a ByID) Len() int           { return len(a) }
func (a ByID) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByID) Less(i, j int) bool { return a[i].ID < a[j].ID }

// BasicDatabase is a simple Database implementation using memory to store data.
type BasicDatabase struct {
	profiles        map[int64]*models.Profile
	activeProfileID *int64
	lastProfileID   int64
	profilesLock    *sync.RWMutex
	pages           map[int64]*models.Page
	activePageID    *int64
	lastPageID      int64
	pagesLock       *sync.RWMutex
}

// NewBasicDatabase creates a new BasicDatabase.
func NewBasicDatabase() *BasicDatabase {
	return &BasicDatabase{make(map[int64]*models.Profile), nil, 0, &sync.RWMutex{},
		make(map[int64]*models.Page), nil, 0, &sync.RWMutex{}}
}

//
// Profile
//

func (db *BasicDatabase) newProfileID() int64 {
	return atomic.AddInt64(&db.lastProfileID, 1)
}

// AddProfile implements Database's AddProfile() method.
func (db *BasicDatabase) AddProfile(profile *models.Profile) (*models.Profile, error) {
	if profile == nil {
		return nil, errors.New(400, "profile must be present")
	}

	db.profilesLock.Lock()
	defer db.profilesLock.Unlock()

	newID := db.newProfileID()
	profile.ID = newID
	db.profiles[newID] = profile

	return profile, nil
}

// DeleteProfile implements Database's DeleteProfile() method.
func (db *BasicDatabase) DeleteProfile(profileID int64) error {
	db.profilesLock.Lock()
	defer db.profilesLock.Unlock()

	_, ok := db.profiles[profileID]
	if !ok {
		return errors.New(404, "Profile not found")
	}
	delete(db.profiles, profileID)
	return nil
}

// FindProfiles implements Database's FindProfiles() method.
func (db *BasicDatabase) FindProfiles() []*models.Profile {
	db.profilesLock.RLock()
	defer db.profilesLock.RUnlock()

	values := make([]*models.Profile, len(db.profiles))
	index := 0
	for _, n := range db.profiles {
		values[index] = n
		index++
	}
	return values
}

// GetActiveProfile implements Database's GetActiveProfile() method.
func (db *BasicDatabase) GetActiveProfile() *models.Profile {
	db.profilesLock.RLock()
	defer db.profilesLock.RUnlock()

	if db.activeProfileID == nil {
		return nil
	}
	profile, ok := db.profiles[*db.activeProfileID]
	if !ok {
		return nil
	}
	return profile
}

// GetProfileByID implements Database's GetProfileByID() method.
func (db *BasicDatabase) GetProfileByID(profileID int64) *models.Profile {
	db.profilesLock.RLock()
	defer db.profilesLock.RUnlock()

	for _, n := range db.profiles {
		if n.ID == profileID {
			return n
		}
	}
	return nil
}

// SetActiveProfile implements Database's SetActiveProfile() method.
func (db *BasicDatabase) SetActiveProfile(profileID int64) (*models.Profile, error) {
	db.profilesLock.Lock()
	defer db.profilesLock.Unlock()

	profile, ok := db.profiles[profileID]
	if !ok {
		return nil, errors.New(404, "Profile not found")
	}
	db.activeProfileID = &profileID
	return profile, nil
}

// UpdateProfile implements Database's UpdateProfile() method.
func (db *BasicDatabase) UpdateProfile(profile *models.Profile) (*models.Profile, error) {
	db.profilesLock.Lock()
	defer db.profilesLock.Unlock()

	_, ok := db.profiles[profile.ID]
	if !ok {
		return nil, errors.New(404, "Profile not found")
	}
	db.profiles[profile.ID] = profile

	return profile, nil
}

//
// Page
//

func (db *BasicDatabase) newPageID() int64 {
	return atomic.AddInt64(&db.lastPageID, 1)
}

// AddPage implements Database's AddPage() method.
func (db *BasicDatabase) AddPage(profileID int64, page *models.Page) (*models.Page, error) {
	if page == nil {
		return nil, errors.New(400, "Page must be present")
	}
	profile := db.getProfile(profileID)
	if profile == nil {
		return nil, errors.New(404, "Profile not found")
	}

	db.pagesLock.Lock()
	defer db.pagesLock.Unlock()

	newID := db.newPageID()
	page.ID = newID
	db.pages[newID] = page

	db.profilesLock.Lock()
	defer db.profilesLock.Unlock()

	profile.Pages = append(profile.Pages, page)

	return page, nil
}

// DeletePage implements Database's DeletePage() method.
func (db *BasicDatabase) DeletePage(profileID int64, pageID int64) error {
	profile := db.getProfile(profileID)
	if profile == nil {
		return errors.New(404, "Profile not found")
	}

	db.pagesLock.Lock()
	defer db.pagesLock.Unlock()

	_, ok := db.pages[pageID]
	if !ok {
		return errors.New(404, "Page not found")
	}
	delete(db.pages, pageID)

	index := db.pageIndex(*profile, pageID)
	if index < 0 {
		return errors.New(404, "Page not found in profile")
	}

	db.profilesLock.Lock()
	defer db.profilesLock.Unlock()
	profile.Pages = append(profile.Pages[:index], profile.Pages[index+1:]...)

	return nil
}

// FindPages implements Database's FindPages() method.
func (db *BasicDatabase) FindPages(profileID int64) ([]*models.Page, error) {
	profile := db.getProfile(profileID)
	if profile == nil {
		return nil, errors.New(404, "Profile not found")
	}

	db.profilesLock.Lock()
	defer db.profilesLock.Unlock()

	return profile.Pages, nil
}

// GetPageByID implements Database's GetPageByID() method.
func (db *BasicDatabase) GetPageByID(profileID int64, pageID int64) (*models.Page, error) {
	profile := db.getProfile(profileID)
	if profile == nil {
		return nil, errors.New(404, "Profile not found")
	}

	db.profilesLock.RLock()
	defer db.profilesLock.RUnlock()

	index := db.pageIndexNoLock(*profile, pageID)
	if index < 0 {
		return nil, errors.New(404, "Page not found in profile")
	}

	return profile.Pages[index], nil
}

// UpdatePage implements Database's UpdatePage() method.
func (db *BasicDatabase) UpdatePage(profileID int64, page *models.Page) (*models.Page, error) {
	profile := db.getProfile(profileID)
	if profile == nil {
		return nil, errors.New(404, "Profile not found")
	}

	db.pagesLock.Lock()
	defer db.pagesLock.Unlock()

	_, ok := db.pages[page.ID]
	if !ok {
		return nil, errors.New(404, "Page not found")
	}
	db.pages[page.ID] = page

	db.profilesLock.Lock()
	defer db.profilesLock.Unlock()

	index := db.pageIndexNoLock(*profile, page.ID)
	if index < 0 {
		return nil, errors.New(404, "Page not found in profile")
	}
	profile.Pages[index] = page

	return page, nil
}

//
// Utils
//

func (db *BasicDatabase) pageIndexNoLock(profile models.Profile, pageID int64) int {
	for i, n := range profile.Pages {
		if n.ID == pageID {
			return i
		}
	}
	return -1
}

func (db *BasicDatabase) pageIndex(profile models.Profile, pageID int64) int {
	db.profilesLock.RLock()
	defer db.profilesLock.RUnlock()

	return db.pageIndexNoLock(profile, pageID)
}

func (db *BasicDatabase) getProfile(profileID int64) *models.Profile {
	db.profilesLock.RLock()
	defer db.profilesLock.RUnlock()
	profile, ok := db.profiles[profileID]
	if !ok {
		return nil
	}
	return profile
}
