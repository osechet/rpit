package database

import "olivierscode.com/rpit/models"

// Database is the interface that defines the database behavior.
type Database interface {
	AddProfile(profile *models.Profile) (*models.Profile, error)
	DeleteProfile(profileID int64) error
	FindProfiles() []*models.Profile
	GetActiveProfile() *models.Profile
	GetProfileByID(profileID int64) *models.Profile
	SetActiveProfile(profileID int64) (*models.Profile, error)
	UpdateProfile(profile *models.Profile) (*models.Profile, error)

	AddPage(profileID int64, page *models.Page) (*models.Page, error)
	DeletePage(profileID int64, pageID int64) error
	FindPages(profileID int64) ([]*models.Page, error)
	GetPageByID(profileID int64, pageID int64) (*models.Page, error)
	UpdatePage(profileID int64, page *models.Page) (*models.Page, error)
}
