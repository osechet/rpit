package database

import (
	"fmt"
	"reflect"
	"sort"
	"sync"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"olivierscode.com/rpit/helpers"
	"olivierscode.com/rpit/models"
)

func TestNewBasicDatabase(t *testing.T) {
	tests := []struct {
		name  string
		check func(db *BasicDatabase) bool
	}{
		{"new db", func(db *BasicDatabase) bool {
			return len(db.profiles) == 0 &&
				db.activeProfileID == nil &&
				db.lastProfileID == 0 &&
				db.profilesLock != nil
		}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewBasicDatabase(); !tt.check(got) {
				t.Errorf("NewBasicDatabase() = %v", got)
			}
		})
	}
}
func TestBasicDatabase_newProfileID(t *testing.T) {
	db := &BasicDatabase{}
	t.Run("One Call", func(t *testing.T) {
		if got := db.newProfileID(); got != 1 {
			t.Errorf("BasicDatabase.newProfileID() = %v, want %v", got, 1)
		}
	})
	db = &BasicDatabase{}
	t.Run("Two Calls", func(t *testing.T) {
		if got := db.newProfileID(); got != 1 {
			t.Errorf("BasicDatabase.newProfileID() = %v, want %v", got, 1)
		}
		if got := db.newProfileID(); got != 2 {
			t.Errorf("BasicDatabase.newProfileID() = %v, want %v", got, 2)
		}
	})
}
func TestBasicDatabase_AddProfile(t *testing.T) {
	name := new(string)
	*name = "profile"
	profile := &models.Profile{Name: name}
	type fields struct {
		profiles        map[int64]*models.Profile
		activeProfileID *int64
		lastProfileID   int64
		profilesLock    *sync.RWMutex
	}
	type args struct {
		profile *models.Profile
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *models.Profile
		wantErr bool
	}{
		{"Invalid input", fields{make(map[int64]*models.Profile), nil, 0, &sync.RWMutex{}}, args{nil}, nil, true},
		{"Valid input", fields{make(map[int64]*models.Profile), nil, 0, &sync.RWMutex{}}, args{profile}, profile, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := &BasicDatabase{
				profiles:        tt.fields.profiles,
				activeProfileID: tt.fields.activeProfileID,
				lastProfileID:   tt.fields.lastProfileID,
				profilesLock:    tt.fields.profilesLock,
			}
			got, err := db.AddProfile(tt.args.profile)
			if (err != nil) != tt.wantErr {
				t.Errorf("BasicDatabase.AddProfile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("BasicDatabase.AddProfile() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBasicDatabase_DeleteProfile(t *testing.T) {
	emptyProfiles := make(map[int64]*models.Profile)
	profilesMap := make(map[int64]*models.Profile)
	for i := 0; i < 5; i++ {
		id := int64(i + 1)
		name := new(string)
		*name = fmt.Sprintf("profile %d", id)
		profile := &models.Profile{ID: id, Name: name, Pages: nil}
		profilesMap[id] = profile
	}
	type fields struct {
		profiles        map[int64]*models.Profile
		activeProfileID *int64
		lastProfileID   int64
		profilesLock    *sync.RWMutex
	}
	type args struct {
		id int64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{"Empty", fields{emptyProfiles, nil, 0, &sync.RWMutex{}}, args{0}, true},
		{"Invalid id", fields{profilesMap, nil, 0, &sync.RWMutex{}}, args{0}, true},
		{"Valid id", fields{profilesMap, nil, 0, &sync.RWMutex{}}, args{1}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := &BasicDatabase{
				profiles:        tt.fields.profiles,
				activeProfileID: tt.fields.activeProfileID,
				lastProfileID:   tt.fields.lastProfileID,
				profilesLock:    tt.fields.profilesLock,
			}
			if err := db.DeleteProfile(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("BasicDatabase.DeleteProfile() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
func TestBasicDatabase_FindProfiles(t *testing.T) {
	profilesMap := make(map[int64]*models.Profile)
	profilesSlice := make([]*models.Profile, 5)
	for i := 0; i < len(profilesSlice); i++ {
		id := int64(i + 1)
		name := new(string)
		*name = fmt.Sprintf("profile %d", id)
		profile := &models.Profile{ID: id, Name: name, Pages: nil}
		profilesMap[id] = profile
		profilesSlice[i] = profile
	}
	sort.Sort(ByID(profilesSlice))
	type fields struct {
		profiles        map[int64]*models.Profile
		activeProfileID *int64
		lastProfileID   int64
		profilesLock    *sync.RWMutex
	}
	tests := []struct {
		name   string
		fields fields
		want   []*models.Profile
	}{
		{"Empty", fields{make(map[int64]*models.Profile), nil, 0, &sync.RWMutex{}}, make([]*models.Profile, 0)},
		{"Not Empty", fields{profilesMap, nil, 0, &sync.RWMutex{}}, profilesSlice},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := &BasicDatabase{
				profiles:        tt.fields.profiles,
				activeProfileID: tt.fields.activeProfileID,
				lastProfileID:   tt.fields.lastProfileID,
				profilesLock:    tt.fields.profilesLock,
			}
			got := db.FindProfiles()
			sort.Sort(ByID(got))
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("BasicDatabase.FindProfiles() = %v, want %v", got, tt.want)
			}
		})
	}
}
func TestBasicDatabase_GetActiveProfile(t *testing.T) {
	profilesMap := make(map[int64]*models.Profile)
	for i := 0; i < 5; i++ {
		id := int64(i + 1)
		name := new(string)
		*name = fmt.Sprintf("profile %d", id)
		profile := &models.Profile{ID: id, Name: name, Pages: nil}
		profilesMap[id] = profile
	}
	type fields struct {
		profiles        map[int64]*models.Profile
		activeProfileID *int64
		lastProfileID   int64
		profilesLock    *sync.RWMutex
	}
	tests := []struct {
		name   string
		fields fields
		want   *models.Profile
	}{
		{"No active profile", fields{make(map[int64]*models.Profile), nil, 0, &sync.RWMutex{}}, nil},
		{"Active profile", fields{profilesMap, &profilesMap[1].ID, 0, &sync.RWMutex{}}, profilesMap[1]},
		{"Invalid active profile", fields{profilesMap, new(int64), 0, &sync.RWMutex{}}, nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := &BasicDatabase{
				profiles:        tt.fields.profiles,
				activeProfileID: tt.fields.activeProfileID,
				lastProfileID:   tt.fields.lastProfileID,
				profilesLock:    tt.fields.profilesLock,
			}
			if got := db.GetActiveProfile(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("BasicDatabase.GetActiveProfile() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBasicDatabase_GetProfileByID(t *testing.T) {
	profilesMap := make(map[int64]*models.Profile)
	for i := 0; i < 5; i++ {
		id := int64(i + 1)
		name := new(string)
		*name = fmt.Sprintf("profile %d", id)
		profile := &models.Profile{ID: id, Name: name, Pages: nil}
		profilesMap[id] = profile
	}
	type fields struct {
		profiles        map[int64]*models.Profile
		activeProfileID *int64
		lastProfileID   int64
		profilesLock    *sync.RWMutex
	}
	type args struct {
		id int64
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *models.Profile
	}{
		{"Empty database", fields{make(map[int64]*models.Profile), nil, 0, &sync.RWMutex{}}, args{}, nil},
		{"Invalid id", fields{profilesMap, nil, 0, &sync.RWMutex{}}, args{0}, nil},
		{"Invalid id", fields{profilesMap, nil, 0, &sync.RWMutex{}}, args{profilesMap[1].ID}, profilesMap[1]},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := &BasicDatabase{
				profiles:        tt.fields.profiles,
				activeProfileID: tt.fields.activeProfileID,
				lastProfileID:   tt.fields.lastProfileID,
				profilesLock:    tt.fields.profilesLock,
			}
			got := db.GetProfileByID(tt.args.id)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("BasicDatabase.GetProfileByID() = %v, want %v", got, tt.want)
			}
		})
	}
}
func TestBasicDatabase_SetActiveProfile(t *testing.T) {
	Convey("SetActiveProfile", t, func() {
		Convey("Given a profile id", func() {
			profile := helpers.NewProfile(1, "profile 1")

			Convey("Given a database without the queried profile", func() {
				db := NewBasicDatabase()
				So(db.profiles, ShouldNotContainKey, profile.ID)

				Convey("When SetActiveProfile is called", func() {
					got, gotErr := db.SetActiveProfile(profile.ID)

					Convey("It should fail, do nothing and return nil", func() {
						So(gotErr, ShouldNotBeNil)
						So(db.activeProfileID, ShouldBeNil)
						So(got, ShouldBeNil)
					})
				})
			})

			Convey("Given a database containing the queried profile", func() {
				db := NewBasicDatabase()
				db.profiles[profile.ID] = profile

				Convey("When SetActiveProfile is called", func() {
					got, gotErr := db.SetActiveProfile(profile.ID)

					Convey("It should success update database and return the new active profile", func() {
						So(gotErr, ShouldBeNil)
						So(db.activeProfileID, ShouldNotBeNil)
						So(*db.activeProfileID, ShouldEqual, profile.ID)
						So(got, ShouldEqual, profile)
					})
				})
			})
		})
	})
}
func TestBasicDatabase_UpdateProfile(t *testing.T) {
	Convey("UpdateProfile", t, func() {
		Convey("Given a profile", func() {
			profile := helpers.NewProfile(1, "profile 1 updated")

			Convey("Given a database not containing the queried profile", func() {
				db := NewBasicDatabase()
				So(db.profiles, ShouldNotContainKey, profile.ID)

				Convey("When UpdateProfile is called", func() {
					got, gotErr := db.UpdateProfile(profile)

					Convey("It should fail and return nil", func() {
						So(gotErr, ShouldNotBeNil)
						So(got, ShouldBeNil)
					})
				})
			})

			Convey("Given a database containg the queried profile", func() {
				db := NewBasicDatabase()
				db.profiles[profile.ID] = profile
				So(db.profiles, ShouldContainKey, profile.ID)

				Convey("When UpdateProfile is called", func() {
					got, gotErr := db.UpdateProfile(profile)

					Convey("It should succeed and return the updated profile", func() {
						So(gotErr, ShouldBeNil)
						So(got, ShouldEqual, profile)
					})
				})
			})
		})
	})
}

//
// Page
//

func TestBasicDatabase_AddPage(t *testing.T) {
	Convey("AddPage", t, func() {
		Convey("Given a profile", func() {
			profile := helpers.NewProfile(1, "profile 1")

			Convey("Given a database", func() {
				db := NewBasicDatabase()

				Convey("When AddPage is called without page argument", func() {
					got, gotErr := db.AddPage(profile.ID, nil)

					Convey("It should fail and return nil", func() {
						So(gotErr, ShouldNotBeNil)
						So(got, ShouldBeNil)
					})
				})
			})

			Convey("Given a page", func() {
				newPage := helpers.NewPage(0, "page")

				Convey("Given a database not containing the specified profile", func() {
					db := NewBasicDatabase()
					So(db.profiles, ShouldNotContainKey, profile.ID)

					Convey("When AddPage is called", func() {
						got, gotErr := db.AddPage(profile.ID, newPage)

						Convey("It should fail and return nil", func() {
							So(gotErr, ShouldNotBeNil)
							So(got, ShouldBeNil)
						})
					})
				})

				Convey("Given a database containg the specified profile", func() {
					db := NewBasicDatabase()
					db.profiles[profile.ID] = profile
					So(db.profiles, ShouldContainKey, profile.ID)

					Convey("When AddPage is called", func() {
						got, gotErr := db.AddPage(profile.ID, newPage)

						Convey("It should succeed and return the created page", func() {
							So(gotErr, ShouldBeNil)
							So(got, ShouldEqual, newPage)
						})
					})
				})
			})
		})
	})
}

func TestBasicDatabase_DeletePage(t *testing.T) {
	Convey("DeletePage", t, func() {
		Convey("Given a page", func() {
			page := helpers.NewPage(1, "page 1")

			Convey("And a profile that contains the page", func() {
				profile := helpers.NewProfile(1, "profile 1")
				profile.Pages = append(profile.Pages, page)
				So(profile.Pages, ShouldContain, page)

				Convey("And a database not containing the specified profile", func() {
					db := NewBasicDatabase()
					So(db.profiles, ShouldNotContainKey, profile.ID)

					Convey("When DeletePage is called", func() {
						gotErr := db.DeletePage(profile.ID, page.ID)

						Convey("It should fail", func() {
							So(gotErr, ShouldNotBeNil)
						})
					})
				})

				Convey("And a database containing the specified profile but not the page", func() {
					db := NewBasicDatabase()
					db.profiles[profile.ID] = profile
					So(db.profiles, ShouldContainKey, profile.ID)
					So(db.pages, ShouldNotContainKey, page.ID)

					Convey("When DeletePage is called", func() {
						gotErr := db.DeletePage(profile.ID, page.ID)

						Convey("It should fail", func() {
							So(gotErr, ShouldNotBeNil)
						})
					})
				})

				Convey("And a database containg the specified profile and the page", func() {
					db := NewBasicDatabase()
					db.profiles[profile.ID] = profile
					So(db.profiles, ShouldContainKey, profile.ID)
					db.pages[page.ID] = page
					So(db.pages, ShouldContainKey, page.ID)

					Convey("When DeletePage is called", func() {
						gotErr := db.DeletePage(profile.ID, page.ID)

						Convey("It should succeed", func() {
							So(gotErr, ShouldBeNil)
						})
					})
				})
			})

			Convey("And a profile that does not contain the page", func() {
				profile := helpers.NewProfile(1, "profile 1")
				So(profile.Pages, ShouldNotContain, page)

				Convey("And a database containg the specified profile and the page", func() {
					db := NewBasicDatabase()
					db.profiles[profile.ID] = profile
					So(db.profiles, ShouldContainKey, profile.ID)
					db.pages[page.ID] = page
					So(db.pages, ShouldContainKey, page.ID)

					Convey("When DeletePage is called", func() {
						gotErr := db.DeletePage(profile.ID, page.ID)

						Convey("It should fail", func() {
							So(gotErr, ShouldNotBeNil)
						})
					})
				})
			})
		})
	})
}

func TestBasicDatabase_FindPages(t *testing.T) {
	Convey("FindPages", t, func() {
		Convey("Given a profile", func() {
			profile := helpers.NewProfile(1, "profile 1")

			Convey("And a database not containing the specified profile", func() {
				db := NewBasicDatabase()
				So(db.profiles, ShouldNotContainKey, profile.ID)

				Convey("When FindPages is called", func() {
					got, gotErr := db.FindPages(profile.ID)

					Convey("It should fail and return nil", func() {
						So(gotErr, ShouldNotBeNil)
						So(got, ShouldBeNil)
					})
				})
			})
		})

		Convey("Given a profile without pages", func() {
			profile := helpers.NewProfile(1, "profile 1")
			So(profile.Pages, ShouldBeEmpty)

			Convey("And a database containing the specified profile", func() {
				db := NewBasicDatabase()
				db.profiles[profile.ID] = profile
				So(db.profiles, ShouldContainKey, profile.ID)

				Convey("When FindPages is called", func() {
					got, gotErr := db.FindPages(profile.ID)

					Convey("It should succeed and return an empty array", func() {
						So(gotErr, ShouldBeNil)
						So(got, ShouldBeEmpty)
					})
				})
			})
		})

		Convey("Given profile with pages", func() {
			profile := helpers.NewProfile(1, "profile 1")
			pages := make([]*models.Page, 0)
			for i := 0; i < 5; i++ {
				pages = append(pages, helpers.NewPage(int64(i+1), fmt.Sprintf("page %d", i+1)))
			}
			profile.Pages = make([]*models.Page, len(pages))
			copy(profile.Pages, pages)
			So(profile.Pages, ShouldNotBeEmpty)

			Convey("And a database containing the specified profile", func() {
				db := NewBasicDatabase()
				db.profiles[profile.ID] = profile
				So(db.profiles, ShouldContainKey, profile.ID)

				Convey("When FindPages is called", func() {
					got, gotErr := db.FindPages(profile.ID)

					Convey("It should succeed and return the list of pages", func() {
						So(gotErr, ShouldBeNil)
						So(got, ShouldResemble, pages)
					})
				})
			})
		})
	})
}

func TestBasicDatabase_GetPageByID(t *testing.T) {
	Convey("GetPageByID", t, func() {
		Convey("Given a profile", func() {
			profile := helpers.NewProfile(1, "profile 1")

			Convey("And a page", func() {
				page := helpers.NewPage(1, "page 1")

				Convey("And a database not containing the specified profile", func() {
					db := NewBasicDatabase()
					So(db.profiles, ShouldNotContainKey, profile.ID)

					Convey("When GetPageByID is called", func() {
						got, gotErr := db.GetPageByID(profile.ID, page.ID)

						Convey("It should fail and return nil", func() {
							So(gotErr, ShouldNotBeNil)
							So(got, ShouldBeNil)
						})
					})
				})

				Convey("And a database containing the specified profile", func() {
					db := NewBasicDatabase()
					db.profiles[profile.ID] = profile
					So(db.profiles, ShouldContainKey, profile.ID)

					Convey("When GetPageByID is called", func() {
						got, gotErr := db.GetPageByID(profile.ID, page.ID)

						Convey("It should failed and return nil", func() {
							So(gotErr, ShouldNotBeNil)
							So(got, ShouldBeNil)
						})
					})

					Convey("And the profile contains the page", func() {
						profile.Pages = append(profile.Pages, page)
						So(profile.Pages, ShouldContain, page)

						Convey("When GetPageByID is called", func() {
							got, gotErr := db.GetPageByID(profile.ID, page.ID)

							Convey("It should succeed and return the page", func() {
								So(gotErr, ShouldBeNil)
								So(got, ShouldEqual, page)
							})
						})
					})
				})
			})
		})
	})
}

func TestBasicDatabase_UpdatePage(t *testing.T) {
	Convey("UpdatePage", t, func() {
		Convey("Given a profile", func() {
			profile := helpers.NewProfile(1, "profile 1")

			Convey("And a page", func() {
				newPage := helpers.NewPage(1, "page 1 update")

				Convey("And a database not containing the specified profile", func() {
					db := NewBasicDatabase()
					So(db.profiles, ShouldNotContainKey, profile.ID)

					Convey("When UpdatePage is called", func() {
						got, gotErr := db.UpdatePage(profile.ID, newPage)

						Convey("It should fail and return nil", func() {
							So(gotErr, ShouldNotBeNil)
							So(got, ShouldBeNil)
						})
					})
				})

				Convey("And the profile contains the page", func() {
					page := helpers.NewPage(1, "page 1")
					profile.Pages = append(profile.Pages, page)
					So(profile.Pages, ShouldContain, page)

					Convey("And a database containing the specified profile and the page", func() {
						db := NewBasicDatabase()
						db.profiles[profile.ID] = profile
						So(db.profiles, ShouldContainKey, profile.ID)
						db.pages[page.ID] = page
						So(db.pages, ShouldContainKey, page.ID)

						Convey("When UpdatePage is called", func() {
							got, gotErr := db.UpdatePage(profile.ID, newPage)

							Convey("It should succeed and return the page", func() {
								So(gotErr, ShouldBeNil)
								So(got, ShouldEqual, newPage)
							})
						})
					})
				})
			})
		})
	})
}
